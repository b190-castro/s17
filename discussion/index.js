console.log("hello world");
/*
	FUNCTIONS:
		functions in javascript are lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked.

		functions are mostly created to create complicated tasks to run several lines of code in succession.

		they are used to prevent repeating lines/blocks of codes that perform the same task/function.
*/


// function declaration
/*
	function - defines a function in javascript; indicator that we are creating a function that will be invoked later in our codes.
	printName() - functionName; functions are named to be able to use later in the code.
	function block {} - the statements inside the curly brace which comprise of the body of the function. This is where the codes are to be executed.

	default syntax:
		function functionName(){
			function statements/code block;		
		};
*/
function printName(){
	console.log("My name is John");
};

// call/invoke the function
/*
	the code block and statements inside a function is not immediately executed when the function is defined/declared. The code block/statements inside a function is executed when the function is invoked/called.

	It is common to use the term "call a function" instead of "invoke a function".
*/
printName();

// results in an error due to the non-existence of the function(the function is not declared).
//declaredFunction();

// DECLARED vs EXPRESSION
// Declaration - using function keyword and functionName
declaredFunction(); // functions can be hoisted
/*
	functions in Javascript can be used before it can be defined, thus hoisting is allowed for the javascript functions
		NOTE: hoisting is javascript's default behavior for certain variable and functions to run/use them before their declaration
*/
function declaredFunction(){
	console.log("Hello from declaredFunction");
};

// FUNCTION EXPRESSION
/*
	a function can also be created by storing it inside a variable

	does not allow hoisting

	a function expression is an anonymous function that is assigned to the variableFunction
		anonymous function - unnamed function
*/
//variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error since technically it is a variable.


let variableFunction = function(){
	console.log("Hello again!");
}

variableFunction();

/*
	miniactivity
		create two functions
			1st function
				log in the console the top three anme tou would like to recommend
			2nd function
				log in the console the top 3 movies you woould like to recommend
		send the output in our chat
*/

function animeRecomm(){
	console.log("Anime Recommendations:")
	console.log("1. Naruto");
	console.log("2. Pokemon");
	console.log("3. Dragon Ball");
};

animeRecomm();

function movieRecomm(){
	console.log("Movie Recommendations:")
	console.log("1. The Notebook");
	console.log("2. The Proposal");
	console.log("3. The Intenrship");
};

movieRecomm();

// Reassigning declared functions
// we can also reassign declared functions and function expressions to new anonymous functions
declaredFunction = function(){
	console.log("updated declaredFunction");
};
declaredFunction();

// However, we cannot change the declared functions/function expressions that are defined using const.

const constFunction = function(){
	console.log("initialized const function");
};
constFunction();

/*constFunction = function(){
	console.log("cannot be re-assigned");
};
constFunction();*/

// Function scoping
/*
	scope is the accessibility of variables/functions

	Javascript variables/functions have 3 scopes
		1. local/block scope - can be accessed inside the curly brace/code block {}.

		2. global scope - outer most part of the codes (it does not belong to any code block and is outside of all curly braces); can be accessed inside any curly brace/code block.
*/

let globalVar = "Mr. Worldwide";
{
	let localVar = "Armando Perez";
	console.log(globalVar);
	console.log(localVar);
}
//console.log(localVar);

/*
	function scope
		-JS has also function scope: each function creates a new scope.
		-Variables inside a function can only be used inside that function
		-variables declared with var, let, and const are quite similar when declared inside a function
*/
function showNames(){
	// Function-scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();
//console.log(functionVar);
//console.log(functionConst);
//console.log(functionLet);
/*
	functionVar, functionConst, and functionLet are function-scoped and cannot be accessed outside the function they are declared
*/

// Nested Functions
/*
	functions that are defined inside another fuction. These nested functions have function scope where they can only be accessed inside the function where they are declared/defined.
*/
function newFunction(){
	let name ="Jane";

	function nestedFunction(){
		let nestedName ="John";
		console.log(name); // valid since we are accessing it inside a nested function that is still isnide the function where "name" is defined
		console.log(nestedName);
	}
	//console.log(nestedName); - returns an error since the nestedName has function scoping; it can only be accessed inside the nestedFunction
	nestedFunction();
}
newFunction();
//nestedFunction(); - return an error since we must call the nestedFunction inside the function where it is declared

/*
	miniactivity
	-create a global-scoped variable
	-create a function
		create another variable
		log in the console the two variables
	-call the function
*/

/*let globalVarVar=function(){
	let globalVarVarVar = "CJC"
	console.log(globalVarVarVar)
}
console.log(globalVarVar)*/

let globalName = "Alex";

function myNewFunction(){
	let nameInside = "Renz";

	console.log(globalName);
	console.log(nameInside);
}
myNewFunction();

// Using alert()
	//alert() - allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It alows us to show a short dialog or instructions to our user. The page will wait (continue to load) until the user dismisses the dialog
alert("Hello World"); //will show as the page loads

//we can use an alert() to show a message to the user from a later function invocation
function showSampleAlert(){
	alert("Hello Again!")
}

//showSampleAlert();
//we will find that thepage waits for the user to dismiss the dialog box before proceeding. We can witness this by reloading the page while the console is open
console.log("I will be displayed after the alert has been closed");

/*
	NOTES ON USING alert()
		-show only an alert() for short dialogs / messages to the user.
		-do not overuse alert() because the program/js has to wait for it to be dismissed before continuing
*/

// using prompt()
	// prompt() - used to allow us to show a small window at the top of the browser to gather user input. Much like alert(), it will have the page wait until the user completes or enters the input. The input from the prompt() will be returned as a string data type once the user dismisses the window.
/*
	Syntax:
		prompt("<dialoginString>")
*/

/*let samplePrompt = prompt("Enter your name.");

console.log(typeof samplePrompt);
console.log("Hello " + samplePrompt);*/

/*let nullPrompt = prompt("do not enter anything here");

console.log(nullPrompt)*/; // prompt() returns an empty string if the user clicks OK button without entering any information and null for users who click the CANCEL button in the window.
/*
	NOTES ON USING prompt()
		it can be used to gather user input and be used in our code. However, since it will have the page wait until the user finished or closed the window, it must not be overused.

		prompt() used globally will run immediately. So for better usage and user experience, it is better to store them inside a function and call whenever they are needed.
*/

/*
	miniactivity
		create a function named welcomeMessage
			create 2 variables
				the firstName of the user that will come from a prompt
				the lirstName of the user that will come from a prompt

			log in the console the message "Hello firstName lastName"

		invoke the function
*/

function welcomeMessage(){
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");

	console.log("Hello " + firstName + " " + lastName);
};

welcomeMessage();

// Function Naming Convention
	// Function names should be definitive of the task that it will perform. It usually contains or starts with a verb/adjective

/*
	1. definitive of the task that it will perform
	2. name your functions with small letters; in cases of multiple words, use camelCasing (underscores are also valid)
*/

function getCourses(){
	let courses = ["Science 101","Math 101","English 101"];
	console.log(courses);
}
getCourses();

// we have to avoid generic names to avoid confusion
function get(){
	let name = "Jamie";
	console.log(name);
};
get();

// avoid pointless and inappropriate name for our functions
function foo(){
	console.log(25%5);
};
foo();
